package frm.antimojang.FRMAntiMojang;

import org.bukkit.plugin.java.JavaPlugin;
import frm.antimojang.FRMAntiMojang.listener.MessageListener;

public final class FRMAntiMojang extends JavaPlugin {

    @Override
    public void onEnable() {
        getServer().getPluginManager().registerEvents(new MessageListener(), this);

    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }
}
