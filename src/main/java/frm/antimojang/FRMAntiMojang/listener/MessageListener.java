package frm.antimojang.FRMAntiMojang.listener;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChatEvent;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;


public class MessageListener implements Listener {

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onPlayerChat(PlayerChatEvent event) {
        event.setCancelled(true);
        for (Player player : Bukkit.getOnlinePlayers()){
            player.sendMessage(event.getMessage());
        }
    }
    
}
